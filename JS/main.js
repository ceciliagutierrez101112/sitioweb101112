 // Import the functions you need from the SDKs you need
 import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
 // TODO: Add SDKs for Firebase products that you want to use
 // https://firebase.google.com/docs/web/setup#available-libraries


 import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";



 // Your web app's Firebase configuration
 const firebaseConfig = {
    apiKey: "AIzaSyBgs9xp9kLT7YxWiNLFk2HEtcHQWOaPjTU",
    authDomain: "administrador-mitski.firebaseapp.com",
    databaseURL: "https://administrador-mitski-default-rtdb.firebaseio.com",
    projectId: "administrador-mitski",
    storageBucket: "administrador-mitski.appspot.com",
    messagingSenderId: "910419675964",
    appId: "1:910419675964:web:0ac71523a78a03e04bec39"
  };

 // Initialize Firebase
 const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();


const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas Variables global
var codigo= 0;
var nombre= "";
var descripcion = "";
var urlImag = "";

function leerInputs() {
  const codigo = document.getElementById('txtCodigo').value;
  const nombre = document.getElementById('txtNombre').value;
  const descripcion = document.getElementById('txtDescripcion').value;
  const urlImag = document.getElementById('txtUrl').value;

  return { codigo, nombre, descripcion, urlImag };
}

function mostrarMensaje(mensaje) {
  const mensajeElement = document.getElementById('mensaje');
  mensajeElement.textContent = mensaje;
  mensajeElement.style.display = 'block';
  setTimeout(() => { mensajeElement.style.display = 'none' }, 1000);
}

//Agregar productos a la DB
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
  alert("Ingrese a add db");
  const { codigo, nombre, descripcion, urlImag } = leerInputs();
  //validar
  if (codigo === "" || nombre === ""  || descripcion === "") {
      mostrarMensaje("Faltaron Datos por Capturar");
      return;
  }
  set(
      refS(db, 'Libros/' + codigo),
      {
          codigo: codigo,
          nombre: nombre,
          descripcion: descripcion,
          urlImag: urlImag
      }
  ).then(() => {
      alert("Se agrego con exito");
      Listarproductos();
  }).catch((error) => {
      alert("Ocurrio un error ")
  })
}

function limpiarInputs() {
  document.getElementById('txtNombre').value = '';
  document.getElementById('txtCodigo').value = '';
  document.getElementById('txtDescripcion').value = '';
  document.getElementById('txtUrl').value = '';
}

function escribirInputs(codigo, nombre, descripcion, urlImag) {
  document.getElementById('txtCodigo').value = codigo;
  document.getElementById('txtNombre').value = nombre;
  document.getElementById('txtDescripcion').value = descripcion;
  document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
  const codigo = document.getElementById('txtCodigo').value.trim();
  if (codigo === "") {
      mostrarMensaje("No se ingreeso un codigo");
      return;
  }

  const dbref = refS(db);
  get(child(dbref, 'Libros/' + codigo)).then((snapshot) => {
      if (snapshot.exists()) {
          const { codigo, nombre, descripcion, urlImag } = snapshot.val();
          escribirInputs(codigo, nombre, descripcion, urlImag);
      } else {
          limpiarInputs();
          mostrarMensaje("El producto con codigo " + codigo + "No Existe.");
      }
  })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Libros');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbref, (snapshot) => {
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);


            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.nombre;
            fila.appendChild(celdaNombre);

            var celdaDescripcion = document.createElement('td');
            celdaDescripcion.textContent = data.descripcion;
            fila.appendChild(celdaDescripcion);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}

Listarproductos();

//Funcion actualizar

function actualizarAutomovil() {
    const { codigo, nombre, descripcion, urlImag } = leerInputs();
    if (codigo === "" || nombre === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la informacion");
        return;
    }
    alert("Actualizar");
    update(refS(db, 'Libros/' + codigo), {
        codigo: codigo,
        nombre: nombre,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);

//Funcion Borrar

function eliminarAutomovil() {
  const codigo = document.getElementById('txtCodigo').value.trim();
  if (codigo === "") {
      mostrarMensaje("No se ingreso un Codigo Valido.");
      return;
  }

  const dbref = refS(db);
  get(child(dbref, 'Libros/' + codigo)).then((snapshot) => {
      if (snapshot.exists()) {
          remove(refS(db, 'Libros/' + codigo)).then(() => {
              mostrarMensaje("Producto eliminado con éxito.");
              limpiarInputs();
              Listarproductos();
          }).catch((error) => {
              mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
          });
      } else {
          limpiarInputs();
          mostrarMensaje("El producto con ID " + codigo + " no existe.");
      }
  });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
        }, (error) => {
            console.error(error);
        }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';

                }, 500);
            }).catch((error) => {
                console.error(error);
            });
        });

    }
});
